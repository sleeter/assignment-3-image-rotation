#ifndef IMAGE_TRANSFORMER_UTILS_H
#define IMAGE_TRANSFORMER_UTILS_H
#include "structures.h"
#pragma once

void free_image(struct image* source);

#endif //IMAGE_TRANSFORMER_UTILS_H
